#!/bin/bash

ln -s .Xresources .Xdefaults
ln -s .profile .bash_profile
ln -s .profile .xprofile
ln -s .profile .zprofile

# automate additional packages and things

pacman -S openssh nfs-utils rofi nodm
systemctl enable sshd

# running this from within the git clone, copy over the nodm configs
cp etc/nodm.conf /etc/
cp etc/pam.d/nodm /etc/pam.d
systemctl enable nodm

mkdir ~/src
cd ~/src
 git clone https://aur.archlinux.org/yay.git
 git clone https://aur.archlinux.org/djmount.git

 echo "Got both yay and djmount -- need to makepkg -si each"

chown -R glaw:wheel /home/glaw



