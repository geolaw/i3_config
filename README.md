# i3_config

My Arch Linux i3 install on my Mac Mini (early 2009)

  This is my customized i3 set up

Credit where credit is due.
Taken from a variety of places
	1.set up off arch on mac mini :
		URL https://gist.github.com/andrsd/cbc85a27f33c77951dff39b2748f23c4
		larbs https://github.com/LukeSmithxyz/LARBS
	2. configs
		https://github.com/lukesmithxyz/
          	http://www.github.com/mdonkers/dotfiles


  My modifications
  merged some i3block scripts ... really dont know wth I am doing with git here
  but here goes :)


  indicator color so I can see where the next window is going to be
  opened
  some modifications for the i3blocks scripts
          pacpackages  - tweaked out -  0 updates (that returned an
  empty string ) and the block was not appearing.
          disk -- added middle click to open a sudo ranger /

	.scripts/tools/remaps ... edit out the remap of caps lock -- either my
	fat fingers or synergy just does not work well with remapping caps lock

  Notes --


Install process :

Installation
1:	Connect to network

	$ wifi-menu to setup wireless networking (assuming DHCP is available)
	-- ethernet --

	b43-open firmware
	ucode11.fw no found  b43-phy0

2.	Setup time

$ timedatectl set-ntp true
Check its status $ timedatectl status

3.     Partition the drive again ;-)

		# cgdisk
		Remove the Arch tmp partition we created earlier.
		Remember to create 128 MB gap after OS X partitions, by typing +128M for the start of the partition.
		Add new Arch boot partition (~ 100 MB)
		Add new Arch root partition (fill the space)
		Swap file can be created later if needed
		Create file systems

		# mkfs.ext2 /dev/<arch boot>
		# mkfs.ext4 /dev/<arch root>
		Mount the partitions

		# mount /dev/<arch root> /mnt
		# mkdir /mnt/boot
		# mount /dev/<arch boot> /mnt/boot


4.     Edit mirror list

		# vim /etc/pacman.d/mirrorlist it will be used pac the next step, so take good care of it here

5. install the base system
		# pacstrap /mnt base

		note - other groups available
		https://www.archlinux.org/groups/
		 note -- start 5:30  done 5:49


6.   Configure the system

		# genfstab -U -p /mnt >> /mnt/etc/fstab

	GEL - extra -- determine the efi partition and add to fstab
	/dev/sda1 /boot/efi	vfat defaults 0 0

		# arch-chroot /mnt /bin/bash
		# echo <computer name> > /etc/hostname
		# ln -s /usr/share/zoneinfo/<zone>/<subzone> /etc/localtime
		# nano /etc/locale.gen uncomment locales you want
GEL : seems important to get this right to make sure all the package builds
			]$ cat /etc/locale.gen  |grep -v ^#
en_US.UTF-8 UTF-8
en_US ISO-8859-1

		# locale-gen
		# echo LANG=<your_locale> > /etc/locale.conf
$ cat /etc/locale.conf
LANG=en_US.UTF-8

		# mkinitcpio -p linux
GEL >> 		# passwd root            # make sure to specify root .. first time I had to boot to single user mode to fix
		# pacman -S gptfdisk
		Boot loader
		# mkdir /boot/efi
		# mount /dev/<EFI partition>
		# pacman -S grub efibootmgr
		# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch

reboot -- welcome to arch

post install

systemctl enable dhcpcd@enp0s10
systemctl start dhcpcd@enp0s10

7.   boot strap in i3  -- https://github.com/LukeSmithxyz/LARBS
		curl -LO lukesmith.xyz/larbs.sh
		bash larbs.sh
start 6:02 PM  -  6:15

	GEL :
		To be able to boot back to macos .. need to add a grub entry :
		add to /etc/grub.d/40_custom file
		menuentry "OS X" {
		  nsmod hfsplus
		    search --set=root --file /System/Library/CoreServices/boot.efi
		    chainloader /System/Library/CoreServices/boot.efi
		}


		# grub-mkconfig -o /boot/grub/grub.cfg

packages to install
pacman -S openssh nodm nfs-utils
systemctl enable sshd
systemctl enable nodm


/etc/pam.d/nodm
/etc/nodm.conf -- tweak with username and stuff

as the user :
git clone https://github.com/LukeSmithxyz/voidrice.git .

for nodm to work, it executes  .xinitrc ... need to chmod +x
chmod +x ~glaw/.xinitrc


mkdir src
cd src
1.	yay - for AUR installs
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si
2.  djmount -- mounts my nas itunes shares
   98  git clone https://aur.archlinux.org/djmount.git
	mkdir ~/nas_music
	djmount ~/nas_music

	[glaw@arch ~]$ ls -lart nas_Music/
	total 6.5K
	dr-xr-xr-x  5 root root   512 Jan  1  2000 HDHomeRun DMS 1049042A
	dr-xr-xr-x  5 root root   512 Jan  1  2000 .debug
	dr-xr-xr-x  7 root root   512 Jan  1  2000 buffalo-nas: LinkStation
	dr-xr-xr-x  5 root root   512 Jan  1  2000 .
	drwx------ 12 glaw wheel 4.0K Dec 22 18:51 ..
	-r--r--r--  1 root root    48 Jan  1  2000 devices



https://github.com/olemartinorg/i3-alternating-layout


Personal stuff :
sudo mkdir /Downloads /Downloads2 /data /data2 /Music /Pictures

Add to /etc/fstab

//192.168.29.10/storage /data2	cifs	defaults,user=glaw,password=XXX,uid=glaw,rw,dir_mode=0777,file_mode=0777,_netdev,noserverino,vers=1.0 0 0
//192.168.29.10/Music /Music	cifs	defaults,user=glaw,password=XXX,uid=glaw,rw,dir_mode=0777,file_mode=0777,_netdev,noserverino,vers=1.0 0 0
//192.168.29.10/Pictures /Pictures cifs	defaults,user=glaw,password=XXX,uid=glaw,rw,dir_mode=0777,file_mode=0777,_netdev,noserverino,vers=1.0 0 0
192.168.29.11:/nfs/TV    /data nfs defaults,vers=3,noatime 0 0
192.168.29.9:/home/media    /plex_media nfs defaults,vers=3,noatime 0 0
192.168.29.6:/Downloads /Downloads nfs defaults,vers=3,noatime 0 0
192.168.29.6:/Downloads2 /Downloads2 nfs defaults,vers=3,noatime 0 0

